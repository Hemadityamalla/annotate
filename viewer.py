import sys

import numpy as np

from PyQt4 import QtGui
from PyQt4 import QtCore

from matplotlib.figure import Figure
from matplotlib.widgets import  RectangleSelector
from matplotlib import cm
from matplotlib.backends.backend_qt4agg \
    import FigureCanvasQTAgg as FigureCanvas
try:
    from matplotlib.backends.backend_qt4agg \
        import NavigationToolbar2QTAgg as NavigationToolbar
except ImportError:
    # Name changes in new versions of the lib
    from matplotlib.backends.backend_qt4agg \
        import NavigationToolbar2QT as NavigationToolbar
    
from PIL import Image

class MplCanvas(FigureCanvas):
    def __init__(self, parent):
        self.fig = Figure()
        self.axes = None
        self.rselector = None

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)

        FigureCanvas.updateGeometry(self)
        

    def load_image(self, fname):
        """ Load the image given by filename. """
        if self.axes == None:
            self.axes = self.fig.add_subplot(111)

        fname = str(fname)
        im = Image.open(fname)
        try:
            self.aim = np.array(im)[::-1, :, :] / 255.
            self.axes.imshow(self.aim, origin='lower', 
                             interpolation='none')
        except IndexError:
            # B/W Image
            self.aim = np.array(im)[::-1, :] / 255.
            self.axes.imshow(self.aim, origin='lower', 
                             interpolation='none',
                             cmap=cm.jet)

        self.fname = fname
        self.draw()


    def add_selector(self, callback):
        rectprops = dict(edgecolor='#bbbbff', lw=1.75, alpha=0.8, fill=False)
        self.rselector = RectangleSelector(self.axes, 
                                           callback, 
                                           drawtype='box', 
                                           rectprops=rectprops)



class MplImageWidget(QtGui.QWidget):
       """ Widget class. """
       def __init__(self, parent=None):
           QtGui.QWidget.__init__(self, parent)
           self.canvas = MplCanvas(parent)
           self.toolbar = NavigationToolbarAnnotate(self.canvas, parent)

           self.vbl = QtGui.QVBoxLayout()

           self.vbl.addWidget(self.canvas)
           self.vbl.addWidget(self.toolbar)

           self.setLayout(self.vbl)


class NavigationToolbarAnnotate(NavigationToolbar):
    save = QtCore.pyqtSignal()

    def save_figure(self, *args):
        self.save.emit()

