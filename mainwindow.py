# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Thu Nov 28 14:12:57 2013
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Annotate(object):
    def setupUi(self, Annotate):
        Annotate.setObjectName(_fromUtf8("Annotate"))
        Annotate.resize(1200, 601)
        Annotate.setAcceptDrops(True)
        self.centralwidget = QtGui.QWidget(Annotate)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.horizontalLayout_2.addWidget(self.tabWidget)
        Annotate.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(Annotate)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1200, 22))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        Annotate.setMenuBar(self.menubar)
        self.actionOpen_image = QtGui.QAction(Annotate)
        self.actionOpen_image.setObjectName(_fromUtf8("actionOpen_image"))
        self.actionSave_annotations = QtGui.QAction(Annotate)
        self.actionSave_annotations.setObjectName(_fromUtf8("actionSave_annotations"))
        self.menuFile.addAction(self.actionOpen_image)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(Annotate)
        self.tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(Annotate)

    def retranslateUi(self, Annotate):
        Annotate.setWindowTitle(_translate("Annotate", "MainWindow", None))
        self.menuFile.setTitle(_translate("Annotate", "File", None))
        self.actionOpen_image.setText(_translate("Annotate", "Open image", None))
        self.actionOpen_image.setShortcut(_translate("Annotate", "Ctrl+O", None))
        self.actionSave_annotations.setText(_translate("Annotate", "Save annotations", None))

