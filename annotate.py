import sys
import os
from collections import namedtuple
import json

import numpy as np

import matplotlib.patches as patches

from PyQt4 import QtGui
from PyQt4 import QtCore
from mainwindow import Ui_Annotate
from imagetab import Ui_ImageTab


Rectangle = namedtuple('Rectangle', ['x0', 'y0', 'x1', 'y1'])

class AnnotateMainWindow(QtGui.QMainWindow, Ui_Annotate):
    def __init__(self, parent = None):
        super(AnnotateMainWindow, self).__init__(parent)

        self.setupUi(self)

        self.actionOpen_image.triggered.connect(self.openImage)
        self.tabWidget.tabCloseRequested.connect(self.closeTab)

    def _open(self, fname):
        try:
            tab = AnnotateTab(fname)
            self.tabWidget.addTab(tab, os.path.split(fname)[-1])
        except IOError as e:
            QtGui.QErrorMessage(self).showMessage(
                "Failed to open file.  Incorrect format? <%s>" % e)

    def openImage(self):
        fnames = QtGui.QFileDialog.getOpenFileNames(self, "Open image file",
                                                 ".",
                                                 "Image files"\
                    "(*.bmp *.png *.tiff *.jpg *.jpeg *.JPG);;"
                                                 "All files (*)")
        if not fnames:
            return

        for fname in [str(fn) for fn in fnames]:
            self._open(fname)


    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def dropEvent(self, event):
        for url in event.mimeData().urls():
            print(url.path())
            self._open(str(url.path()))
            event.acceptProposedAction()


    def closeTab(self, index):
        self.tabWidget.removeTab(index)


class AnnotateTab(QtGui.QWidget, Ui_ImageTab):
    def __init__(self, fname, parent = None):
        super(AnnotateTab, self).__init__(parent)
        
        self.setupUi(self)
        
        self.listWidget.itemClicked.connect(self.onitemclicked)
        self.listWidget.itemChanged.connect(self.onitemchanged)

        self.setFocus()

        self.fname = fname

        self.imageWidget.canvas.load_image(fname)
        self.imageWidget.canvas.add_selector(self.onselect)
        self.imageWidget.toolbar.save.connect(self.onsave)

        self.fnotes = os.path.splitext(self.fname)[0] + '.notes'
        try:
            self.load_notes()
        except IOError:
            # No available notes: no problem.
            pass

        self.current_selection = None

    def iterAnnotations(self):
        for i in range(self.listWidget.count()):
            yield self.listWidget.item(i)


    def onselect(self, eclick, erelease):
        x0, x1 = sorted([eclick.xdata, erelease.xdata])
        y0, y1 = sorted([eclick.ydata, erelease.ydata])

        rect = Rectangle(x0, y0, x1, y1)
        annotation = Annotation("add annotation", rect, 
                                self.imageWidget.canvas)

        self.listWidget.addItem(annotation)
        self.onsave()


    def onitemclicked(self, selection):
        if self.current_selection is not None:
            self.current_selection.selected = False

        selection.selected = True
        self.current_selection = selection


    def onitemchanged(self, item):
        self.onsave()


    def onsave(self):
        annotations = [{'box': a.rect, 'text': str(a.text())} for a in
                       self.iterAnnotations()]
        outdict = {'filename': self.fname, 'annotations': annotations}

        with open(self.fnotes, 'w') as fout:
            json.dump(outdict, fout, sort_keys=True,
                      indent=4, separators=(',', ': '))

    def load_notes(self):
        with open(self.fnotes) as fin:
            d = json.load(fin)

        for a in d['annotations']:
            annotation = Annotation(a['text'], Rectangle(*a['box']),
                                    self.imageWidget.canvas)
            
            self.listWidget.addItem(annotation)
            

    def keyPressEvent(self, event):
        if event.key() in {QtCore.Qt.Key_Backspace, QtCore.Qt.Key_Delete}:
            self.current_selection.delete()
            self.listWidget.takeItem(self.listWidget.currentRow())
            self.onsave()


class Annotation(QtGui.QListWidgetItem):
    def __init__(self, text, rect, canvas=None):
        super(Annotation, self).__init__(text)

        self.rect = rect

        self.setFlags(self.flags() | QtCore.Qt.ItemIsEditable)
        self.canvas = canvas

        self.patch = patches.Rectangle((rect.x0, rect.y0), 
                                       rect.x1 - rect.x0, 
                                       rect.y1 - rect.y0, 
                                       fill=False, ec='w')
        self.canvas.axes.add_patch(self.patch)

        self.selected = False

        if self.canvas is not None:
            self.canvas.draw()

    def set_selected(self, status):
        self._selected = status
        if status:
            self.patch.set_edgecolor('w')
            self.patch.set_linewidth(2.0)

        else:
            self.patch.set_edgecolor('#777777')
            self.patch.set_linewidth(1.0)

        self.canvas.draw()

    def get_selected(self):
        return self._selected

    selected = property(fget=get_selected, fset=set_selected)

    def delete(self):
        self.patch.remove()
        self.canvas.draw()


def main():
    qApp = QtGui.QApplication(sys.argv)
    aw = AnnotateMainWindow()

    aw.show()
    aw.raise_()

    return qApp.exec_()

if __name__ == '__main__':
    sys.exit(main())

