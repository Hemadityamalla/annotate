import sys
from collections import namedtuple
import json

from numpy import *
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.patches as patches

Rectangle = namedtuple('Rectangle', ['x0', 'y0', 'x1', 'y1'])

def main():
    fname = sys.argv[1]
    with open(fname) as fin:
        notes = json.load(fin)

    fimage = notes['filename']
    im = Image.open(fimage)
    aim = array(im)[::-1, :, 0]

    plt.figure(figsize=(9, 4.75))
    left = plt.axes([0.1, 0.1, 0.5, 0.8])
    right = plt.axes([0.625, 0.1, 0.275, 0.8])

    left.imshow(aim, origin='lower', 
               interpolation='none',
                cmap=cm.jet)

    bg_box = list(find_annotations(notes['annotations'], 'background'))[0]
    bg_z, bg_sect = crop(aim, bg_box)
    bg_avg = average(bg_sect, axis=0)
    bg_std = std(bg_sect, axis=0)
    plot_rect(left, bg_box, ec='b', lw=2.0)

    top, bottom = 0, 10000
    for rect in find_annotations(notes['annotations'], 'column'):
        styles = {'column': {'ec': 'w', 'lw': 1.0},
                  'background': {'ec': 'b', 'lw': 2.0}}

        z, sect = crop(aim, rect)
        plot_rect(left, rect, ec='w', lw=1.0)                
                
        top = max(top, rect.y1)
        bottom = min(bottom, rect.y0)

        c_avg = average(sect, axis=0)
        right.plot(c_avg, z, linewidth=2.0, c='#777777')
                
        i0, i1 = z[0] - bg_z[0], z[-1] - bg_z[0] + 1

        sect = where(sect >= bg_avg[newaxis, i0:i1] + bg_std[newaxis, i0:i1], 
                     sect, bg_avg[newaxis, i0:i1])

        c_avg = average(sect, axis=0)
        c_avg[:] -= bg_avg[i0: i1]
        right.plot(c_avg, z, linewidth=2.0, c='#ff7777')

    bottom = 100 * floor(bottom / 100.)
    top = 100 * ceil(top / 100.)

    left.axhline(bottom, ls='--', c='w')
    left.axhline(top, ls='--', c='w')
    left.set_xlabel("i")
    left.set_ylabel("j")

    right.set_ylim([bottom, top])
    right.yaxis.tick_right()
    right.yaxis.set_label_position("right")
    right.set_xlim([0, 150])
    right.set_ylabel("j")
    right.set_xlabel("Intensity [a.u.]")

    right.grid()

    plt.show()


def plot_rect(ax, rect, **kwargs):
    patch = patches.Rectangle((rect.x0, rect.y0), 
                              rect.x1 - rect.x0, 
                              rect.y1 - rect.y0, 
                              fill=False, **kwargs)
    
    ax.add_patch(patch)


def find_annotations(notes, text):
    """ Iterates over all annotations in a file with the text "column". """
    for annotation in notes:
        if annotation['text'] == text:
            yield Rectangle(*annotation['box'])


def crop(aim, box):
    """ Extract from an image the pixels in a box. """
    ibox = round_box(box)
    
    z = r_[ibox.y0: ibox.y1]
    # The image is read transposed, so is axes are (y, x)
    sect = aim[ibox.y0: ibox.y1, ibox.x0: ibox.x1].T
    
    return z, sect


def round_box(box):
    """ Rounds some box coordinates to integers. """
    imin = int(floor(min(box.x0, box.x1)))
    imax = int(ceil(max(box.x0, box.x1)))

    jmin = int(floor(min(box.y0, box.y1)))
    jmax = int(ceil(max(box.y0, box.y1)))

    return Rectangle(imin, jmin, imax, jmax)


if __name__ == '__main__':
    main()

    
    
